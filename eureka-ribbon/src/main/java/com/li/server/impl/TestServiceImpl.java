package com.li.server.impl;

import com.li.server.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public String test() {
        System.out.println("我是消费者");
        restTemplate.getForObject("http://EUREKA-CLIENT/index?name="+"张三",String.class);
        return null;
    }

    @Override
    public void get() {
        String url = "http://eureka-client/index?name={name}";
        Map<String,String> param = new HashMap<>();
        param.put("name","张三");
        restTemplate.getForEntity(url,String.class,param);
    }

    @Autowired
    private LoadBalancerClient loadBalancer; // 获取注册中心的所有实例

    @Override
    public void post() {
        // 获取所有访问的服务名
        ServiceInstance instance = loadBalancer.choose("EUREKA-CLIENT");
        // 拼接访问的路径
    }
}
