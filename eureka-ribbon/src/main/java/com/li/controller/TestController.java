package com.li.controller;

import com.li.server.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
    @Autowired
    private TestService testServiceImpl;

    @RequestMapping("/ribbon")
    @ResponseBody
    public void test(){
        testServiceImpl.test();
    }
}
