package com.li.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ClientController {

    @RequestMapping("/index")
    @ResponseBody
    public void con(String name){
        System.out.println("这里是生产者"+name);
    }
}
